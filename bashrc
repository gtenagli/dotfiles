# ~/.bashrc: executed by bash(1) for non-login shells.

umask 066

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# set hostname string
myhn=${HOSTNAME%%.*}
case $myhn in
        dbvrts1063)          myhn="\[\e[1;30m\]syscontrol-dev" ;;
        itrac1111|itrac1121) myhn="\[\e[1;31m\]db-manager" ;;
        itrac1214)           myhn="\[\e[1m\]syscontrol-build" ;;
        itrac12*)            myhn="\[\e[1;30m\](RAC12)\[\e[1;37m\]${myhn}" ;;
        itrac11*)            myhn="\[\e[1;30m\](RAC11)\[\e[1;37m\]${myhn}" ;;
        itrac10*)            myhn="\[\e[1;30m\](RAC10)\[\e[1;37m\]${myhn}" ;;
        aiadm*)              myhn="\[\e[46;1;1m\][AIadm]\[\e[;1;37m\]" ;;
        lxadm*)              myhn="\[\e[44;1;1m\][LXadm]\[\e[;1;37m\]" ;;
        *tenaglia*|nuc8eval) myhn="\[\e[1;35m\][${myhn}]\[\e[;1;37m\]" ;;
        *)                   myhn="\[\e[1;37m\]${myhn}";;
esac

if [ -n "$ISDOCKERCONTAINER" ]; then
    myhn="\[\e[1;30m\]docker"
fi

# set username string
myuser=${USER}
myprsym='$'
case $myuser in
        root)   myuser="\[\e[41;1;37m\]root\[\e[;0;37m\]"; myprsym='#' ;;
        oracle) myuser="\[\e[1;31m\]oracle\[\e[;0;37m\]" ;;
        sysctl) myuser="\[\e[46;1;37m\]sysctl\[\e[;0;37m\]" ;;
        *)      myuser="${myuser}";;
esac

# Note PS1 to be kept in sync below when adding the git prompt
export PS1="\[\e[0m\]${myuser}@${myhn}:\[\e[1;34m\]\w\[\e[0m\]${myprsym} "
export PS2="\[\e[1m\]>\[\e[0m\] "

# git completion and prompt

# for mac
if [ -d /usr/local/etc/bash_completion.d/ ]; then
    . /usr/local/etc/bash_completion.d/git-prompt.sh
    . /usr/local/etc/bash_completion.d/git-completion.bash
fi

export GIT_PS1_SHOWCOLORHINTS=yes
export GIT_PS1_SHOWDIRTYSTATE=yes
export GIT_PS1_SHOWUNTRACKEDFILES=yes
export GIT_PS1_SHOWUPSTREAM=auto

function _git_prompt() {
    local git_status="`git status -unormal 2>&1`"
    if ! [[ "$git_status" =~ Not\ a\ git\ repo ]]; then
        if [[ "$git_status" =~ nothing\ to\ commit ]]; then
            local ansifg=32
            local ansibg=42
        elif [[ "$git_status" =~ nothing\ added\ to\ commit\ but\ untracked\ files\ present ]]; then
            local ansifg=34
            local ansibg=44
        else
            local ansifg=33
            local ansibg=43
        fi
        echo -n '\[\e[1;33;'"$ansifg"'m\]'"$(__git_ps1)"'\[\e[0m\]'
    fi
}

# Note: the Kopano environment var is defined in the Kopano management script
function _prompt_command() {
    date
    # Check kerberos ticket presence
    if ! $(klist -s)
    then if [ "${myuser}" != "root" ] && [ "${myuser}" != "oracle" ] && [ "${myuser}" != "sysctl" ]
        then
            myuser="\[\e[;0;31m\]${myuser}\[\e[;0;31m\]"
        fi
    else
        myuser=${USER}
    fi
    PS1="\[\e[0m\]${myuser}@${myhn}:\[\e[1;34m\]\w\[\e[0m\]$(_git_prompt)${KOPANOENV}${myprsym} "
}
PROMPT_COMMAND=_prompt_command

### if this is an xterm set the title to user@host:dir
##case "$TERM" in
##xterm*|rxvt*)
##    PROMPT_COMMAND='date; echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
##    ;;
##*)
##    PROMPT_COMMAND='date'
##    ;;
##esac


# history matters!
export HISTSIZE=2000
export HISTFILESIZE=2000
# don't put duplicate lines
export HISTCONTROL=ignoredups

# shell options
shopt -s \
	cdspell \
	checkhash \
	checkwinsize \
        histappend \
	hostcomplete \
	huponexit \
	no_empty_cmd_completion \
	sourcepath \
        histappend
set -o vi

# Editor is VIM by default
export EDITOR="/usr/bin/vim"
export VISUAL=$EDITOR

# Browser is Chromium
export BROWSER="/usr/bin/chromium"

# aliases are on separate file
if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# custom completion
if [ -d ~/.bash_completion.d/ ]; then
    . ~/.bash_completion.d/*
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# lang
#export LANG="C"
export LC_CTYPE="en_US.UTF-8"
export LANG="en_US.UTF-8"
export LC_ALL="en_US.UTF-8"

# path stuff
#PATH=$JAVA_HOME/bin:$PATH
PATH="$HOME/bin":$PATH
PATH="$HOME/go/bin":$PATH
PATH=$PATH:"/sbin"
PATH=$PATH:"/usr/sbin"
PATH=$PATH:"/usr/local/sbin"
PATH=$PATH:"/usr/X11R6/bin"
PATH=$PATH:"/usr/kerberos/bin"
export PATH

# Standard ls colours

# OS-specific section
myos=`uname`

if [ "$TERM" != "dumb" ]; then
    eval `dircolors -b`
    alias ls='ls --color=auto'
fi

case $myos in
    Linux*)
        # Powerline
        . /usr/share/powerline/bindings/bash/powerline.sh
    ;;
    Darwin*)
        # homebrew path stuff
        export PATH="/usr/local/opt/openssl/bin:$PATH"
        export PATH="/usr/local/opt/gettext/bin:$PATH"
        export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
        export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
    ;;
esac

