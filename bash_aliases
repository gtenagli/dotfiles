alias cern-proxy-off='export http_proxy="" https_proxy=""'
alias cern-proxy-on='export http_proxy=socks5://127.0.0.1:8666 https_proxy=socks5://127.0.0.1:8666'

# ls
alias ls='ls --color=auto'
alias l='ls -lh'
alias ll='ls -Alh'
alias la='ls -A'

# config
alias cdmod='cd ~/src/ai/mod'
alias cdhg='cd ~/src/ai/hg'
