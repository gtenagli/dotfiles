" Useful stuff to remember
" gj gk: move by row, not by line. Still undecided whether to do the mapping or not:
" nnoremap j gj
" nnoremap k gk
" nnoremap gj j
" nnoremap gk k

colors default

set vb t_vb=
set tw=72
set bs=2

set bg=dark

syntax on
filetype on
set smartindent
set nocompatible
set autoindent
set shiftround
set smarttab
set showmatch
set matchtime=1
set ruler
set ignorecase
set smartcase
set incsearch
set hls
set expandtab
set nobackup
" break lines at words
set linebreak
" shell completion made human
set wildmode=longest,list
"set nowrap
set nonumber
" split below/rigt of current pane instead of above/left
set splitbelow
set splitright
" faster timeout - let's try this
set timeoutlen=50

" Automatically reload .vimrc when changing
autocmd! bufwritepost .vimrc source %

highlight WhitespaceEOL ctermbg=darkyellow guibg=red
match WhitespaceEOL /\s\+$/

map <F11> :set paste!<CR>
set pastetoggle=<F11>
map <F2> :w<CR>
imap <F2> <C-O>:w<CR>
nmap <F8> :q!
imap <F8> <C-O>:q!
nmap <F10> :x<CR>
imap <F10> <C-O>:x<CR>
nmap <F5> dd
imap <F5> <C-O>dd
map <S-Up> <C-W>k
imap <S-Up> <C-O><C-W>k
map <S-Down> <C-W>j
imap <S-Down> <C-O><C-W>j
map <S-Tab> :bn<CR>
imap <S-Tab> <C-O>:bn<CR>

" PEP 8
autocmd BufNewFile,BufRead *.py
    \ set tabstop=4
    \ | set softtabstop=4
    \ | set shiftwidth=4
    \ | set textwidth=79
    \ | set expandtab
    \ | set autoindent
    \ | set fileformat=unix

autocmd BufNewFile,BufRead *.js, *.html, *.css
    \ set tabstop=2
    \ | set softtabstop=2
    \ | set shiftwidth=2

" perl support
let g:Perl_AuthorName      = 'Giacomo Tenaglia'
let g:Perl_AuthorRef       = 'jack'
let g:Perl_Email           = 'giacomo.tenaglia@gmail.com'
"let g:Perl_Company         = ''

" keep the cursor in place when joining lines
nnoremap J mzJ`z

" center screen after various actiona
nnoremap n nzz
nnoremap } }zz

" sudo write file
command W w !sudo tee % > /dev/null

" consistent Y yanks from cursor to EOL
nnoremap Y y$

set encoding=utf-8

" MacVim-specific configuration
" TODO: add conditional include
set guifont=Fixed:h15

" plugins & packages
packloadall

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
set laststatus=2
set t_Co=256
" only using airline so get rid of the mode showing
set noshowmode

let python_highlight_all=1
